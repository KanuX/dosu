            /========================\
            | ###    ###   ###   # # |
            | #  #   # #   #     # # |
            | #  #   # #   ###   # # |
            | #  #   # #     #   # # |
            | ###    ###   ###   ### |
            \========================/
                   by KanuX-14

DoSu is a legacy automated script to run command as root user.

/-----\
|Usage|
\-----/

* To run the command it is simple:
>> dosu 'your command here'

* To add it to the binary folder:
>> sh -c "chmod +x dosu.sh && ./dosu.sh 'cp -v dosu.sh /sbin/dosu'"

/----\
|Why?|
\----/

Well, I like to keep things simple.
If I can run a very basic script to run super-user commands...
I just use this one. ;)