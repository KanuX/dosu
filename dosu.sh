#/bin/sh

function helpMe
{
    printf "DoSu is a legacy automated script to run command as root user.\nUsage: dosu \'your command here\'\n"
}

if [ "$#" -eq 0 ]; then
    helpMe
    exit
fi

ARGUMENTS=""

for i; do
    ARGUMENTS="$ARGUMENTS $i"
done

su -c "${ARGUMENTS}"

# I said, the script is very basic.